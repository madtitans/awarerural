# AwareRural

## 1. Introduction
This application aims to complete the challenge posed under “Technology as an enabler for Rural Distribution System”, sub-theme of the theme “General”, for the Rural Development Hackathon, 2018. This application aims to eliminate the sanitation problems in the rural and backward areas of the nation. Sanitation generally refers to the provision of facilities and services for the safe disposal of human urine and feces. Inadequate sanitation is a major cause of disease world-wide and improving sanitation is known to have a significant beneficial impact on health both in households and across communities. The word 'sanitation' also refers to the maintenance of hygienic conditions, through services such as garbage collection and wastewater disposal.

**Our proposed solution aims to eliminate these issues faced by the people**.

- Our Solution engages common people of the area. A platform can be created which will allow the people of the area to be able to upload an "Image" of the contaminated area, like garbage block behind the society's uncommon lane or anywhere which is affecting the surroundings.
- The "Image" will have the location details automatically and an issue will be raised to the designated authority and then the assigned authorities will have a fixed number of days to resolve the issue at hand.
- In case of the failure, the problem along with the picture will be automatically sent to the higher authority.
- After that it will be forwarded to the area head, to the city or the state head and so on according to the hierarchy set in case of continuous ignorance or failure.
- Even in a case where there is no such facility available, user can raise a request and submit the online application for new such services in their area from the authority through our app.
- To make things more effective and transparent, all these issues will be available to general public via website, which can be accessed globally.

More Information and Wireframe about the Proposed Application is available with Attached Presentation.

High-Resolution Wireframe of the Proposed Application is available on this [Link](https://drive.google.com/file/d/1fQ1lPZuHi2dfVSAbwi3JSILkBo9-NeX4/view).

Whole Documentation of the proposed Application is available on this [Google Drive Link](https://drive.google.com/open?id=1IvYgj0A3-rXgooimp5NWkVdm2oY1M83I).

## 2.   Objectives
  - User Friendly Interface
    -    A more engaging and easy-to-use graphical user interface for all types of users, ranging from developers to non-technical sound users.
    -    Easy Login/Signup and pages to create and view New Records.

 - Creating New Records
    -    Application provides clear and detailed description on how to create new records by Users.
    -    Users can click the photos of the person and fill in the required information and submit it to the database.

- Authority Record-viewing
    -    Authority can view the records and go in contact with the selected personnel.
    -    Authority can provide the appropriate response and solution to the personnel and close the required Record with appropriate documents uploaded.

- Publically available Data
    -    All the data uploaded by Authority/Users is publically available on a Website for people to see. Thus, maintaining transparency and discrepancy.
    -    People can choose to question the data uploaded by authorities and can reopen the Record if not satisfied with the Data.

## 3.   Features
This prototype application contains some of the Industry-standard features such as 
- Material Design.
- Faster Bootup.
- Easier Login and Signup.
- Ability to create New Records.
- Upload Records to Database in real-time.
- Authority can change the Application status.
- Near Real-time data update.
- List of all records available in One-go.
- Website support to publically view Data.
- Uses APIs to get the content in real time.

# 4.    Running The Project
Let  us remind you again that the minimum Android OS that you need to run this project is Lollipop (API Level 21). So, make sure you're satisfying the minimum requirements first. Otherwise, your handset won't be able to parse the apk file.

- ### Permissions Required :
    This application requires you to provide few permissions to it, in order to work properly. Here's the list of permissions that the application needs :
    - Internet Access
    - View WiFi Connections
    - Storage (Read/Write Perms For Cache)
    - Read Google Service Configuration
    - Camera (For Taking Pictures)

- #### Instructions For Direct APK Installation :
    If you want to run this application on your android phone, please move over to the "[`Downloads`](https://bitbucket.org/madtitans/awarerural/downloads/)" section and download the latest stable APK build for your android phone. You do not need any external libraries or application.

- #### Instructions For Developers/Testers :
     If you're a developer or any user who wishes to test this application and run this android project, it's recommended to install Visual Studio with Xamarin Support and Android SDKs on your system. Remember that Android SDKs should be in your local path for you to be able to compile the project properly. You can find the source code in the "[SOURCE](https://bitbucket.org/madtitans/awarerural/src/)" directory.

    If you do not happen to have Visual Studio, it is recommended to get it because it'll download all the required packages on its own, if they're not present. You can use Visual Studio's Free Community Edition. It'll work, as we've developed this application on it.
But, if for some reason, you don't want to or can't install Visual Studio, you will need to have .NET, Xamarin, Android SDK and required Packages in your system's local path for you to be able to compile and execute this application project.

You can check the Demonstration Video On YouTube :

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/x_KEYGsToHI/hqdefault.jpg)](https://youtu.be/x_KEYGsToHI)

## 5.   Technology Stack Used
- ### Front End Development

    - ##### User Interface design:
       - Android XML (.axml)

    - ##### Other Softwares: 
       - GIMP

- ### Back End Development

    - ##### Main programming language:
       - C# - to develop base functionalities of proposed application.

    - ##### Basic scripting languages:
       - PHP - for Parsing back-end

-    ### Frameworks

       - Xamarin Framework to develop native portable Android Application in C#
       - .NET Framework to use Microsoft' Visual Studio and C# basic packages.

-    ### Packages

       - C# Basic Packagesâ€“ provided by Microsoft.
       - Newtonsoft To parse JSON data.
       - Google Play Services Packages provided by Google for Android Development.

## 6.  Team's Area of Expertise
- ### Front End Developer
    - **Ankit Passi** **(** UI/UX Designer **)**
        - Photoshop
        - Adobe XD
        - GIMP
        - Illustrator
        - C#
        - C++
        - Xamarin Framework
        - VR Developer
        - Well-versed with Unreal Engine Visual Scripting.


- ### Back End Developers
    - **Dhruv Kanojia** **(** Lead Developer **)**
        - Python
        - C#
        - Core Java
        - JSON
        - PHP
        - .NET Framework
        - Xamarin Framework
        - Web Development
        - Google Accessibility Packages


- ### Website Designer
    - **Shivangi Mittal**
        - HTML
        - CSS
        - Bootstrap
        - Website Designing

## 7.  Team
### Team Name : MadTitans
- #### [Dhruv Kanojia](https://bitbucket.org/Xonshiz/) (Lead Developer)
- #### [Ankit Pass](https://bitbucket.org/ankitpassi141/) (UI/UX Designer)
- #### [Shivangi Mittal](https://bitbucket.org/Shivangi48/) (Website Designer)