﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpingHand.models
{
    class user_PastEntries
    {
        public string b_id { get; set; }
        public string b_street_address { get; set; }
        public string b_status { get; set; }
        public string b_status_text { get; set; }
        public string b_photo { get; set; }
        public string b_area_Code { get; set; }
        public string b_age { get; set; }
        public string b_family_count { get; set; }
        public string b_extraInfo { get; set; }
        public string b_reason { get; set; }
        public string b_ngo_name { get; set; }
    }
}
