﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpingHand
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Application_aboutApplication : ContentPage
	{
		public Application_aboutApplication ()
		{
			InitializeComponent ();
            aboutApplication_mainLogo.Source = MainPage.primaryDomain + "/images/mainBanner_2.png";

        }

        void helpingHandWebsite_Tap(object sender, EventArgs args)
        {
            var labelSender = (Label)sender;
            Device.OpenUri(new Uri(MainPage.helpingHandDomain));
            //DisplayAlert("Alert", "Tap gesture recoganised", "OK");
        }
    }
}