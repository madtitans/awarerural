﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpingHand
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class users_MainScreen : TabbedPage
    {
        public users_MainScreen ()
        {
            InitializeComponent();
        }

        async void aboutUs_Activated(Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Application_aboutUs());
        }

        async void aboutApplication_Activated(Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Application_aboutApplication());
        }

        void aboutWebsite_Activated(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri(MainPage.helpingHandDomain));
        }

    }
}